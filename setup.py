from setuptools import setup, find_packages
import sys, os

version = '0.8'

setup(
    name='ckanext-opendatahub',
    version=version,
    description="OpenDataHub Theme",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='ckan, opendatahub, theme',
    author='groundrace',
    author_email='groundrace@me.com',
    url='http://www.sciamlab.com',
    license='GNU Affero GPL v3 (AGPL)',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.opendatahub'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points='''
        [ckan.plugins]
        opendatahub = ckanext.opendatahub.plugin:OpenDataHubPlugin
        # Add plugins here, e.g.
        # myplugin=ckanext.opendatahub.plugin:PluginClass
    ''',
)
