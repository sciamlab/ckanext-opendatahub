===========================================
ckanext-opendatahub extension for CKAN 2.2+
===========================================

This extension contains plugins used to customize the default CKAN
for the http://www.opendatahub.it web site

Community
---------

* `Issue tracker <https://github.com/sciamlab/ckanext-opendatahub/issues>`_

Contributing
------------

For contributing to ckanext-opendatahub or its documentation,
please drop an email to support@opendatahub.it

Copying and License
-------------------

This material is copyright (c) 2013-2015 SciamLab.

It is open and licensed under the GNU Affero General Public License (AGPL) v3.0
whose full text may be found at:
http://www.fsf.org/licensing/licenses/agpl-3.0.html

