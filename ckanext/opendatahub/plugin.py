import os
import mimetypes
import routes.mapper
import sys
import urllib2

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.lib.helpers as helpers

from logging import getLogger

from pylons import request
from pylons import config
import collections
from ckan.lib.base import request
from ckan.lib.base import c, g, h
from ckan.lib.base import model
from ckan.lib.base import render
from ckan.lib.base import _
from ckan.common import json

from ckan.controllers.user import UserController

from geomet import wkt, InvalidGeoJSONException

log = getLogger(__name__)

def convert_to_wkt(geojson):
    try:
        return wkt.dumps(json.loads(geojson))
    except Exception, inst:
        msg = "Error converting geojson to wkt: %s" % (inst)
        raise Exception, msg

def dscount():
    url = config.get('ckan4j.webapi_url', '')+'/ext/datasets/count'
    try:
        response = urllib2.urlopen(url)
        response_body = response.read()
    except Exception, inst:
        msg = "Couldn't connect to dscount service %r: %s" % (url, inst)
        raise Exception, msg
    try:
        dscount = json.loads(response_body)
    except Exception, inst:
        msg = "Couldn't read response from dscount service %r: %s" % (response_body, inst)
        raise Exception, inst
    return dscount['count']

def dsstats():
    url = config.get('ckan4j.webapi_url', '')+'/ext/datasets/stats'
    try:
        response = urllib2.urlopen(url)
        response_body = response.read()
    except Exception, inst:
        msg = "Couldn't connect to dsstats service %r: %s" % (url, inst)
        raise Exception, msg
    try:
        dsstats = json.loads(response_body)
    except Exception, inst:
        msg = "Couldn't read response from dsstats service %r: %s" % (response_body, inst)
        raise Exception, inst
    return dsstats

def publisher_reports(publisher):
    url = config.get('ckan4j.webapi_url', '')+'/ext/publisher/'+publisher+'/reports'
    try:
        response = urllib2.urlopen(url)
        response_body = response.read()
    except Exception, inst:
        msg = "Couldn't connect to publisher reports service %r: %s" % (url, inst)
        raise Exception, msg
    try:
        result = json.loads(response_body)
    except Exception, inst:
        msg = "Couldn't read response from publisher reports service %r: %s" % (response_body, inst)
        raise Exception, inst
    return result

#def categories():
#    url = config.get('ckan4j.webapi_url', '')+'/ext/categories?count=true'
#    try:
#        response = urllib2.urlopen(url)
#        response_body = response.read()
#    except Exception, inst:
#        msg = "Couldn't connect to categories service %r: %s" % (url, inst)
#        raise Exception, msg
#    try:
#        categories = json.loads(response_body)
#    except Exception, inst:
#        msg = "Couldn't read response from categories service %r: %s" % (response_body, inst)
#        raise Exception, inst
#    return collections.OrderedDict()

def get_package_category(package_extras):
    for item in package_extras:
        if (item.get("key") == 'theme'):
            try:
                themes = json.loads(item.get("value"))
                #print('PAOLO',themes[0])
                return themes[0]
            except Exception, inst:
                for item in package_extras:
                    if (item.get("key") == 'category'):
                        return item.get("value")
    for item in package_extras:
        if (item.get("key") == 'category'):
            return item.get("value")
    return 'OTHER'


class OpenDataHubPlugin(plugins.SingletonPlugin):
   """
   read the configuration
   (no configuration for the moment)
   """
   # Declare that this class implements IConfigurer.
   plugins.implements(plugins.IConfigurer, inherit=True)
   plugins.implements(plugins.IRoutes, inherit=True)
   plugins.implements(plugins.IFacets)
   plugins.implements(plugins.ITemplateHelpers)

   def get_helpers(self):
      return {'publisher_reports':publisher_reports,
	#'categories':categories,
	'dscount':dscount, 'dsstats':dsstats, 'get_package_category':get_package_category,
	'convert_to_wkt':convert_to_wkt}

   def dataset_facets(self, facets_dict, package_type):
      d = collections.OrderedDict()
      d['organization'] = _('Organizations')
#      d['category'] = _('Categories')
      d['groups'] = _('DCAT Themes')
      d['tags'] = _('Tags')
      d['res_format'] = _('Formats')
      d['license_id'] = _('Licenses')
      return d

   def organization_facets(self, facets_dict, organization_type, package_type):
      d = collections.OrderedDict()
#      d['category'] = _('Categories')
      d['groups'] = _('DCAT Themes')
      d['tags'] = _('Tags')
      d['res_format'] = _('Formats')
      d['license_id'] = _('Licenses')
      return d

   def group_facets(self, facets_dict, organization_type, package_type):
      d = collections.OrderedDict()
      d['organization'] = _('Organizations')
#      d['category'] = _('Categories')
      d['tags'] = _('Tags')
      d['res_format'] = _('Formats')
      d['license_id'] = _('Licenses')
      return d


   def update_config(self, config):
      # Add this plugin's templates dir to CKAN's extra_template_paths, so
      # that CKAN will use this plugin's custom templates.
      # 'templates' is the path to the templates dir, relative to this
      # plugin.py file.
      toolkit.add_template_directory(config, 'templates')
      toolkit.add_public_directory(config, 'public')
      toolkit.add_resource('public', 'ckanext-opendatahub')

      # add media type for common extensions non included in the default mimetype
      #mimetypes.add_type('application/json', '.geojson')

   def before_map(self, map):
      with routes.mapper.SubMapper(map, controller='ckanext.opendatahub.plugin:SciamLabUserController') as m:
          m.connect('user_profile','/user/profile/{id}',
              #controller='ckanext.opendatahub.controller:SciamLabUserController',
              action='profile',
              ckan_icon='user')

      map.connect('analytics','/analytics', controller='ckanext.opendatahub.plugin:SciamLabAnalyticsController',
                  action='index')
      return map

class SciamLabUserController(UserController):

   def profile(self):
       context = {'user_profile': True, 'user': c.user or c.author,
                  'auth_user_obj': c.userobj}
       data_dict = {'user_obj': c.userobj}
       self._setup_template_variables(context, data_dict)
       return render('user/profile.html')

class SciamLabAnalyticsController(toolkit.BaseController):

   def index(self):
       return toolkit.render('analytics/index.html')
