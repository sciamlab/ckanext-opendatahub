var QueryString = function () {
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = pair[1];
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], pair[1] ];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(pair[1]);
		}
	} 
	return query_string;
} ();


// tooltip for eurovoc dendogram
var div = d3.select("body").append("div")   
.attr("class", "tooltip")               
.style("opacity", 0);


var c_width = $("#cloud").width(),
c_height = ($("#cloud").height()<40) ? 440 : $("#cloud").height();
//console.log("CWIDTH/CHEIGH:"+c_width+"/"+c_height);

var cwidth = c_width;
var cheight = c_height;

var myRequest = null;
var myRequestCount = null;

var json = null;
var words_array = new Array();
var count_array = new Array();

var fill = d3.scale.category20();

function draw(words) {
	d3.select("#cloud").append("svg")
	.attr("id","graph_svg")
	.attr("width", cwidth)
	.attr("height", cheight)
	.append("g")
	.attr("transform", "translate("+c_width/2+","+c_height/2+")")
	.selectAll("text")
	.data(words)
	.enter().append("text")
	//.style("class", "click")
  //.style("position", "absolute")
	.style("font-size", function(d) { return d.size + "px"; })
	.style("font-family", "Impact")
	.style("fill", function(d, i) { return fill(i); })
	.style("cursor", "pointer")
	.style("cursor","hand")
	.attr("text-anchor", "middle")
	.on("click", function(d) { 
		window.location.href=""+"dataset?tags="+d.text;
		//alert(d.text); 
	})
	.attr("transform", function(d) {
		return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
	})
	.text(function(d) { return d.text; });
}

function createCloud(data) {

	for(var i = 0 ; i<data.length ; i++){
		words_array[i] = data[i].text;
		count_array[i] = data[i].size;
		//if(i>80) break;
	}

	d3.layout.cloud()
	.size([cwidth*0.95, cheight*0.95])
	.words(words_array.map(
					function(d) {
						var size = count_array[words_array.indexOf(d)];
						var newsize;
						if (size>=500) {
							newsize = Math.round(size*0.07);
						} else if (size>=300 && size<500 ){
							newsize = Math.round(size*0.11);
						} else {
							newsize = Math.round(size*0.25);
						}
						return {text: d, size: newsize};
					}
			)
	)
	.rotate(
			function() { 
				return ~~(Math.random() * 5) * 30 - 60; 
				return 0;
			})
			.font("Impact")
			.fontSize(function(d) { return d.size; })
			.on("end", draw)
			.start();
}

function make_eurovoc_dendogram() {
	var radius = c_width/2;

	var colorx = d3.scale.category20b();

	var cluster = d3.layout.cluster().size([360, radius - 220]);

	var diagonal = d3.svg.diagonal.radial().projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

	var svg = d3.select("#cloud").append("svg")
	.attr("width", radius*2)
	.attr("height", radius*2 )
	.append("g")
	.attr("transform", "translate(" + (radius-20) + "," + (radius-20) + ")");

	d3.json("/ckan4j/webapi/ext/eurovoc/stats", function(error, root) {
		var nodes = cluster.nodes(root);
		var link = svg.selectAll("path.link")
		.data(cluster.links(nodes))
		.enter().append("path")
		.attr("class", "link")
		.attr("d", diagonal);

		var node = svg.selectAll("g.node")
		.data(nodes)
		.enter()
		.append("svg:a")
		.attr("xlink:href", function(d){
			if (d.children) {
				if (d.name!="EurovocClassificationBubble") 
					return "/opendatahub/dataset?extras_eurovoc-field-label="+d.name; 
			} else {
				return "/opendatahub/dataset?extras_eurovoc-microthesaurus-label="+d.name
			}
		}) 
		.append("g")
		.attr("class", "node")
		.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })



		node.append("circle")
		.attr("r", function(d) { return d.children ? 12 : 4.5; })
		.style("fill", function(d) { return (d.children ? (d.name!="EurovocClassificationBubble" ? colorx(d.name) : "transparent") : "#C6DBEF"); });

		node.append("text")
		.attr("dy", ".31em")
		.attr("text-anchor", function(d) { return ( d.x < 180 ? (d.children ? "middle" : "start") : (d.children ? "middle" : "end")); })
		.attr("transform", function(d) { return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)"; })
		.text(function(d) { return (d.name!="EurovocClassificationBubble" ? (d.children ? d.name : (d.x < 180 ? "("+d.size+") " + d.name : d.name+ " ("+d.size+")")) : ""); })
		.style("font-weight",function(d) { return d.children ? "bold" : "normal"; })
		.on("mouseover", function(d) {      
			div.transition()        
			.duration(200)      
			.style("opacity", 1);      
			div .html(d.name )  
			.style("left", (d3.event.pageX) + "px")     
			.style("top", (d3.event.pageY - 28) + "px");    
		})
		.on("mouseout", function(d) {       
			div.transition()        
			.duration(500)      
			.style("opacity", 0);   
		});

	});

	d3.select(self.frameElement).style("height", radius * 2 + "px");
}

function make_dcat_bubble() {
var diameter = 700,
    format = d3.format(",d"),
    color = d3.scale.category20c();

var bubble = d3.layout.pack()
    .sort(null)
    .size([diameter, diameter])
    .padding(1.5);

var svg = d3.select("#cloud").append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble");

d3.json("/ckan4j/webapi/ext/category/facets", function(error, root) {
//d3.json("/flare.json", function(error, root) {
  if (error) throw error;
  var node = svg.selectAll(".node")
      .data(bubble.nodes(loop(root))
//      .data(bubble.nodes(classes(root))
      .filter(function(d) { return !d.children; }))
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.append("title")
      .text(function(d) { return d.className + ": " + format(d.value); });

  node.append("circle")
      .attr("r", function(d) { return d.r; })
      .style("fill", function(d) { return color(d.packageName); });

  node.append("text")
      .attr("dy", ".3em")
      .style("text-anchor", "middle")
      .text(function(d) { return d.className.substring(0, d.r / 3); });
});

function loop(root){
var classes = [];
for(var i=0 ; i<root.length ; i++){
	classes.push({packageName: root[i].name, className: root[i].display_name, value: root[i].count});
}
  return {children: classes};

}

// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
  var classes = [];

  function recurse(name, node) {
    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
    else {
	var el = {packageName: name, className: node.name, value: node.size};
	classes.push(el);
    }
  }

  recurse(null, root);
  return {children: classes};
}

d3.select(self.frameElement).style("height", diameter + "px");
}

if (QueryString.t=='eurovoc' ) {
	make_eurovoc_dendogram();
	document.getElementById("eurovoc-li").className = "active";
	document.getElementById("wordcloud-li").className = "";
//	document.getElementById("dcat-li").className = "";
} else if(QueryString.t=='dcat') {
	make_dcat_bubble();
	document.getElementById("wordcloud-li").className = "";
	document.getElementById("eurovoc-li").className = "";
//	document.getElementById("dcat-li").className = "active";
} else {
	$.ajax({
  		url: "/ckan4j/webapi/ext/tags/top?limit=80"
	}).done(function(data) {
		var tags = data.tags;
		var cloud = [];
		for(var i=0 ; i<tags.length ; i++){
			cloud[i] = {'text': tags[i].tag, 'size':tags[i].count};
		}		
		createCloud(cloud);
	});
	document.getElementById("wordcloud-li").className = "active";
	document.getElementById("eurovoc-li").className = "";
//	document.getElementById("dcat-li").className = "";
}

