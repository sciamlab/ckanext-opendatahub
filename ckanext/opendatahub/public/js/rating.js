var user = $('#username').html();
var ds = $('#ds').html();
if(user != ''){
    //enabling the rating
    $('#5').click(function (){ rate(5)});
    $('#4').click(function (){ rate(4)});
    $('#3').click(function (){ rate(3)});
    $('#2').click(function (){ rate(2)});
    $('#1').click(function (){ rate(1)});
    $('#hint-user').show();
    $('#hint-nouser').hide();
}else{
    //if no user is logged in the rating is disabled
    $('.rating-new').toggleClass("active");
    $('#hint-user').hide();
    $('#hint-nouser').show();
}
//getting current rate
$.ajax({
	url: '/ckan4j/webapi/rate/'+ds
}).done(function(data) {
	updateRating(data.rating, data.count);
});

function rate(star_num){    
	$.ajax({
		url: '/ckan4j/webapi/rate/'+ds+'?user='+user+'&rating='+star_num,
		method: "POST"
	}).done(function(data) {
		updateRating(data.rating,data.count);
	});
}

function updateRating(rating, count){
        $('#rating_count').html(count);
        //reset current rating
        $("#5").removeClass();
        $("#4").removeClass();
        $("#3").removeClass();
        $("#2").removeClass();
        $("#1").removeClass();
        if(rating>0 && rating<6){
	        $('#'+rating).addClass("selected");
	        $('#rating_value').html($("#label_"+rating).html());
	}else{
		$('#rating_value').html('-');
	}
}
