(function render() {
    // Attach a click listener to the social login buttons to trigger the auth flow.
    //GOOGLE
    var signinButton = document.getElementById('customBtn');
    signinButton.addEventListener('click', function() {
    window.location.href = 
    'https://accounts.google.com/o/oauth2/auth?'+
    'scope=email' +
    '&redirect_uri=http://'+window.location.hostname+'/ckan4j/webapi/auth/gplus' +
    '&response_type=code' +
    '&client_id=690094214896-0pu7c2i84f5eddu5eosfrekpf59t27bd.apps.googleusercontent.com';
    });

    //FACEBOOK
    var signinButtonFb = document.getElementById('customBtnFb');
    signinButtonFb.addEventListener('click', function() {
    window.location.href = 
    'https://www.facebook.com/dialog/oauth?'+
    'client_id=849520311729195'+
    '&scope=email,public_profile' +
    '&response_type=code' +
    '&redirect_uri=http://'+window.location.hostname+'/ckan4j/webapi/auth/facebook';
    });

    //GITHUB
    var signinButtonGh = document.getElementById('customBtnGh');
    signinButtonGh.addEventListener('click', function() {
    window.location.href =
    'https://github.com/login/oauth/authorize?'+
    'client_id=e293a3eaa9de990f1c80'+
    '&redirect_uri=http://'+window.location.hostname+'/ckan4j/webapi/auth/github';
    });

})();


